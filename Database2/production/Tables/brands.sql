CREATE TABLE [production].[brands] (
    [brand_id]   INT           IDENTITY (1, 1) NOT NULL,
    [brand_name] VARCHAR (255) NOT NULL,
   
    [test_col2] VARCHAR(50) NULL, 
    [test_col3] INT NULL, 
    PRIMARY KEY CLUSTERED ([brand_id] ASC)
);

